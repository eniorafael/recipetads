import React, { Component, useState } from 'react';
import { View, StyleSheet } from 'react-native';
import { Input, Button } from 'react-native-elements';
//import UserContext from '../context/UserContext';

const SignInScreen = (props) => {
  const [username, setUsername] = useState('eniorafael');
  const [password, setPassword] = useState('123456789')

  return(
    <>
    <View style={styles.container}>
      <Input
        placeholder='Usuário'
        onChangeText={(text) => setUsername(text)}
        value={username}
      />
      <Input
        placeholder='Senha'
        value={password}
        onChangeText={(text) => setPassword(text)}
        secureTextEntry
      />
      <Button
        containerStyle={styles.button}
        title="Entrar"
        type="outline"
        onPress={() => props.navigation.navigate('Home', {
          userName:username
        })}        
      /> 
    </View>
    {/* <View style={{alignContent:'flex-end'}}>
    <Button
      title="NOVO USUÁRIO"
      type="clear"
      onPress={}        
    />    
  </View>*/}
  </>   
  )
}

const styles = StyleSheet.create({
  container:{
    justifyContent:'center',
    alignItems:'center',
    flex:1,
  },
  button:{
    paddingTop:10,
    width:'80%'
  }
})

SignInScreen.navigationOptions = (props) => ({
  title:'Tela de Login'
})

export default SignInScreen;