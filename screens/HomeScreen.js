import React, { Component, useState, useContext, useEffect } from 'react';
import { View, Text, StyleSheet, TouchableHighlight, Image, AsyncStorage, Share, ScrollView } from 'react-native';
import { DrawerNavigator } from 'react-navigation'; 
import { Button, Icon, ListItem } from 'react-native-elements';

const HomeScreen = (props) => {
  const [recipes, setRecipes] = useState([]);
  const [userInfo, setUserInfo] = useState({});

  addNewRecipe = (data) => {
    const { state } = props.navigation;

    const newItems = [
      ...recipes, {
      title:data.title,
      description:data.description,
      imageUri:data.imageUri,
      userName:state.params.userName,
      createdAt:new Date(),
      likes:0
    }];

    AsyncStorage.setItem('recipes', JSON.stringify(newItems));
    setRecipes(newItems);
  }

  useEffect(() => {
    retrieveData();
  }, [])  

  const retrieveData = async () => {
    try {
      const data = await AsyncStorage.getItem('recipes');
      console.log(data)
      setRecipes(data ? JSON.parse(data) : []);
    } catch(error) {
      console.log('error')
    }
  };  

  const handleShare = async (item) => {
    try {
      const result = await Share.share({
        message:`${item.title} - ${item.description}`
      });

    } catch (error) {
      alert(error.message);
    }
  };  

  const handleRemove = (item) => {
    const newItems = recipes.filter(r => r.createdAt !== item.createdAt);
    AsyncStorage.setItem('recipes', JSON.stringify(newItems));
    setRecipes(newItems);  
  }

  const handleLike = (item, position) => {
    const recipe = {
      ...item,
      likes: item.likes + 1
    }

    const newItems = [...recipes];
    newItems[position] = recipe;

    AsyncStorage.setItem('recipes', JSON.stringify(newItems));
    setRecipes(newItems);      
  }

  return(    
    <>
      <ScrollView style={styles.listContainer}>
        {
          recipes.filter(item => item !== null).map((item, i) => (
            <ListItem
              key={i}
              title={item.title}
              subtitle={
                <>
                  <Text style={styles.subtitle}>{`Criado por: ${item.userName}`}</Text>
                  <Text style={styles.subtitle}>{`${new Date(item.createdAt).toLocaleDateString()} às ${new Date(item.createdAt).toLocaleTimeString()}`}</Text>
                </>
              }
              onLongPress={() => handleRemove(item)}
              onPress={() => props.navigation.navigate('FormRecipe', {
                formData:item
              })}
              leftAvatar={{ source:{uri: item.imageUri}, size:'large' }}
              rightIcon={
                <>
                <Text>
                  {item.likes}
                </Text>
                <Icon 
                  name='heart'
                  type='material-community'
                  color={item.likes ? 'red' : 'black'}
                  iconStyle={styles.buttonAction}
                  onPress={() => handleLike(item, i)}
                />
                <Icon 
                  name='share-variant'
                  type='material-community'
                  iconStyle={styles.buttonAction}
                  onPress={() => handleShare(item)}
                /> 
                </>             
              }
              bottomDivider
              //chevron
            />
          ))
        }
      </ScrollView>
      <View style={styles.fabContainer}>
        <Icon
          reverse
          name='plus'
          size={30}
          type='material-community'
          color='#517fa4'
          onPress={() => props.navigation.navigate('FormRecipe', {
            addNewRecipe:addNewRecipe
          })}
        />    
      </View>
    </>
  )
}

HomeScreen.navigationOptions = (props) => ({
  // headerLeft: () => (
  //   <Icon 
  //     iconStyle={{paddingLeft:5}}
  //     name='menu' 
  //     onPress={props.navigation.openDrawer}
  //   />
  // ),
  headerRight: () => (
    <Icon 
      iconStyle={{paddingLeft:5}}
      name='signOut' 
      onPress={props.navigation.openDrawer}
    />
  ),
  title:`Olá ${props.navigation.getParam('userName')}`
})

const styles = StyleSheet.create({
  listContainer:{
    height:'70%'
  },
  fabContainer:{
    height:'30%', 
    flex:1, 
    justifyContent:'center', 
    alignItems:'center'
  },
  buttonAction:{
    paddingLeft:8
  },
  subtitle:{
    fontSize:12
  }
})

export default HomeScreen;