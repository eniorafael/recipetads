import React, { Component, useState, useEffect } from 'react';
import { View, StyleSheet, TouchableHighlight, Image } from 'react-native';
import { Input, Button, Icon } from 'react-native-elements';

const FormRecipeScreen = (props) => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [imageUri, setImageUri] = useState('');
  const [dataFound, setFound] = useState(false);

  const {navigation} = props;

  const addImage = (imageUri) => {    
    setImageUri(imageUri);
  }

  useEffect(() => {
    const formData = props.navigation.getParam('formData', null);
    if (formData !== null){
      setTitle(formData.title);
      setDescription(formData.description);
      setImageUri(formData.imageUri);
      setFound(true);
    };
    
  }, [])

  return(
    <View style={styles.container}>      
        <View style={styles.imageCard}>
          {imageUri ?
            <Image
              style={styles.imageDisplay}
              source={{uri:imageUri}}
            />
           : 
            <Icon 
              name='camera'
              type='material-community'
              size={40}
              color='#b8b1b0'
              onPress={() => navigation.navigate('Camera', {setImage:addImage })}
            />            
         }     
        </View>      
      <Input
        placeholder='Titulo'
        onChangeText={(text) => setTitle(text)}
        value={title}
        disabled={dataFound}
      />
      <Input
        multiline
        numberOfLines={5}
        placeholder='Descrição'
        onChangeText={(text) => setDescription(text)}
        value={description}
        disabled={dataFound}
      />      
      {!dataFound &&
        <Button
          containerStyle={styles.button}
          title="Adicionar"
          type="outline"
          onPress={() => {
            const { addNewRecipe } = navigation.state.params;    
            addNewRecipe({imageUri, title, description})
            navigation.goBack();          
          }}        
        />
      } 
    </View>
  )
}

FormRecipeScreen.navigationOptions = (props) => ({
  title: props.navigation.state.params.formData ? 'Visualizar Receita' : 'Incluir Receita',
})

const styles = StyleSheet.create({
  container:{
    //justifyContent:'center',
    paddingTop:20,
    alignItems:'center',
    flex:1,
  },
  button:{
    paddingTop:10,
    width:'90%'
  },
  imageCard:{
    width:'50%',
    height:'30%',
    borderWidth:1,
    borderColor:'#b8b1b0',
    alignItems:'center',
    justifyContent:'center',
    marginBottom:30
  },
  imageDisplay:{
    width:'100%',
    height:'100%',    
  }
})

export default FormRecipeScreen;