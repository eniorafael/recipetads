import React from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import * as Permissions from 'expo-permissions';
import { Camera } from 'expo-camera';
import { Icon } from 'react-native-elements';

class CameraScreen extends React.Component {
  state = {
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
    photo: '',
  };

  static navigationOptions = {
    title:'Capturar Foto',
  };  

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }

  takePicture() {
    // this.setState({
    //     takeImageText: "... PROCESSING PICTURE ..."
    // });
    //alert('take picture');
    const { navigation } = this.props;
    //const { setFormImage } = navigation.state.params;

    //const setImage = navigation.getParam('setFormImage');
    //console.log(setImage);
    const { setImage } = navigation.state.params;
    //console.log(addImage);

    this.camera.takePictureAsync({ skipProcessing: true })
      .then((data) => {
        this.setState({photo: data.uri})
        setImage(data.uri);
        navigation.goBack();
      });    
  }

  render() {
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Camera 
            style={{ flex: 1 }} 
            type={this.state.type}
            ref={ref => { this.camera = ref }}
          >
            <View
              style={{
                flex: 1,
                backgroundColor: 'transparent',
                flexDirection: 'row',
              }}>
              {/* <TouchableOpacity
                style={{
                  flex: 0.1,
                  alignSelf: 'flex-end',
                  alignItems: 'center',
                }}
                onPress={() => {
                  this.setState({
                    type:
                      this.state.type === Camera.Constants.Type.back
                        ? Camera.Constants.Type.front
                        : Camera.Constants.Type.back,
                  });
                }}>                
              </TouchableOpacity> */}
              <View style={styles.buttonCamera}>
                  <Icon 
                    name='camera'
                    type='material-community'
                    size={40}
                    color='white'
                    onPress={this.takePicture.bind(this)}
                  /> 
                </View>                
            </View>
          </Camera>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  buttonCamera:{
    flex:0.5, 
    flexDirection:'row', 
    flexGrow:1, 
    alignSelf:'flex-end',  
    justifyContent:'center', 
    alignItems:'center'    
  }
})

export default CameraScreen;