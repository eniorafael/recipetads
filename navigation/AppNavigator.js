import React from 'react';
import { createAppContainer, createSwitchNavigator, createDrawerNavigator, createStackNavigator } from 'react-navigation';

// import HomeScreen from '../screens/HomeScreen';
// import MainTabNavigator from './MainTabNavigator';

import MainTabNavigator from './MainTabNavigator';
import SignInNavigator from './SignInNavigator';

// const drawerNavigator = createDrawerNavigator({
//   Home: {
//     screen: MainTabNavigator,
//   },
//   Sair: {
//     screen: SignInNavigator,
//   },
// },{
//   initialRouteName:'Sair'
// });

export default createAppContainer(MainTabNavigator);
