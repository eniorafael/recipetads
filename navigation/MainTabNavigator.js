import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import HomeScreen from '../screens/HomeScreen';
import FormRecipeScreen from '../screens/FormRecipeScreen';
import CameraScreen from '../screens/CameraScreen';
import SignInScreen from '../screens/SignInScreen';

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
    FormRecipe: FormRecipeScreen,
    Camera:CameraScreen,
    Sair:SignInScreen
  },{
    initialRouteName:'Sair'
  }
);

export default HomeStack;
